#Configuration and settings file!
#Help: PASS: change the password to one of your choice (set the same in the server php file).
#Help: DEBUG_WEBSEND: shows debugging messages for easier tracking of bugs.
#Help: SALT: adds a salt to the hashed password when sending over bukkit -> php connection.
PASS=idkfa99
#Optional settings. Remove the '#' to use.
URL=https://www.t0rched.com/api/commands/get.php
WEBLISTENER_ACTIVE=true
ALTPORT=25590
DEBUG_WEBSEND=true
#GZIP_REQUESTS=false/true
